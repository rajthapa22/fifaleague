﻿app.controller('history', function ($scope, $http) {
    function getHistory() {
        $http.get("api/match/history")
            .success(function (data) {
                console.log(data);
                $scope.matches = data.reverse();
            }).
            error(function (data) {
                console.log(data);
            }
            );
    };
    getHistory();

    $scope.searchedPlayer = "";

    $scope.searchHistory = function () {
        if ($scope.searchedPlayer === '') {
            alert('Please enter the name of player, you want to search for');
            return false;
        }
        else {
            $http.get("api/match/history/" + $scope.searchedPlayer)
                .success(function(data) {
                    console.log(data);
                    if (data !== "") {
                        $scope.matches = data.reverse();
                    } else {
                        alert('player not found');
                        $scope.searchedPlayer = "";
                    }
                }).
                error(function(data) {
                        console.log(data);
                    }
                );
        }
    }
})