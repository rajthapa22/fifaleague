﻿app.controller('league', function ($scope, $http) {
    function getUsers() {
        $http.get("api/user/getusers")
            .success(function(data) {
                console.log(data);
                $scope.users = data;
            }).
            error(function(data) {
                    console.log(data);
                    console.log('error');
                }
            );
    };
    getUsers();

    function getLeagueTable() {
        $http.get("api/league/table")
            .success(function(data) {
                $scope.leagueTable = data;
            }).
            error(function(data) {
                    console.log(data);
                    console.log('error');
                }
            );
    }

    getLeagueTable();
})