﻿app.controller('matchReport', function ($scope, $http) {
    $scope.users = "";

    function getUsers() {
        $http.get("api/user/getusers")
            .success(function (data) {
                console.log(data);
                $scope.users = data;
            }).
            error(function (data) {
                console.log(data);
                console.log('error');
            }
            );
    }
    getUsers();

    $scope.currentDateTime = new Date();
    function player() {
        this.Name = "";
        this.Team = "";
        this.Goals = 0;
    }

    function matchReport() {
        this.Player1 = $scope.player1;
        this.Player2 = $scope.player2;
        this.winner = $scope.winner;
        this.MatchDate = new Date();
    }

    $scope.player1 = new player();
    $scope.player2 = new player();
    $scope.winner = "";

    $scope.Winners = [];

    $scope.WinnerOptions = function () {
        $scope.Winners = [];
        $scope.Winners.push($scope.player1.Name);
        $scope.Winners.push($scope.player2.Name);
    }

    function isValidPlayer(p) {
        if (p.Name === '' || p.Team === '' || p.Goals === '') {
            return false;
        }
        return true;
    }

    function isValidmatchReport(matchReport) {
        var validPlayer1 = isValidPlayer(matchReport.Player1);
        if (validPlayer1) {
            var validPlayer2 = isValidPlayer(matchReport.Player2);
            if (validPlayer2) {
                if (matchReport.winner === '' ){
                    alert('Winner is invalid');
                    return false;
                }
            } else {
                alert('Player2 is invalid');
                return false;
            }
        } else {
            alert('Player1 is invalid');
            return false;
        }
        return true;
    }

    $scope.addMatchReport = function () {
        console.log($scope.player1);
        console.log($scope.player2);
        console.log($scope.winner);
        var report = new matchReport();

        if (isValidmatchReport(report)) {
            $http.post('api/match/report', report)
                .success(function () {
                    alert('Match report has been successfully added');
                }).error(function () {
                    alert('There was an error adding match report');
                });
        }
    }
});