﻿app.controller('user', function ($scope, $http) {
    $scope.users = [];

    function getUsers() {
        $http.get("api/user/getusers")
            .success(function (data) {
                console.log(data);
                $scope.users = data;
            }).
            error(function (data) {
                console.log(data);
                console.log('error');
            }
            );
    }
    getUsers();

    $scope.userName = "";

    $scope.addUser = function () {
        if ($scope.userName === '') {
            alert('Please provide a name.');
            return false;
        }

        if ($scope.userName.split(' ').length > 1) {
            alert('Just add your first name');
            return false;
        }

        var playerAlreadyAdded = false;
        angular.forEach($scope.users, (function (x) {
            if (x.Name.toLowerCase() === $scope.userName.toLowerCase()) {
                playerAlreadyAdded = true;
            }
        }));

        if (!playerAlreadyAdded) {
            $http.post("api/user/adduser", { Name: $scope.userName })
                .success(function(data) {
                    $scope.userName = "";
                    $scope.users.push(data);
                    alert('player ' + data.Name + ' added');
                }).
                error(function(data) {
                        console.log(data);
                        console.log('error');
                    }
                );
        }
        else {
            alert($scope.userName + ' is already added. Please give different name');
            $scope.userName = "";
        }
    }
});