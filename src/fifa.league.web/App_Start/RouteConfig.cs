﻿using System.Web.Mvc;
using System.Web.Routing;

namespace fifa.league.web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("MatchReport", "match/{report}", new { controller = "MatchReport", action = "Index" });

            routes.MapRoute("user", "user/{add}", new { controller = "User", action = "Index" });

            routes.MapRoute("history", "history/{name}", new { controller = "History", action = "Index" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
