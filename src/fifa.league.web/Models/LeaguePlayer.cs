﻿using System;

namespace fifa.league.web.Models
{
    public class LeaguePlayer
    {
        public  string Name { get; set; }
        public int GameWon { get; set; }
        public int GameLoss { get; set; }
        public int GoalScored { get; set; }
        public int GoalConceded { get; set; }

        public decimal WinPercentage { get; set; }
    }
}