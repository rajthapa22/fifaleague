﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace fifa.league.web.Models
{
    public class Match
    {
        [JsonIgnore]
        [BsonElement("id")]
        public ObjectId Id { get; set; }
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
        public string Winner { get; set; }
        public DateTime? MatchDate { get; set; }
    }
}