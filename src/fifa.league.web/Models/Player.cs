﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace fifa.league.web.Models
{
    public class Player
    {
        [BsonElement("id")]
        [JsonIgnore]
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public int Goals { get; set; }
        public string Team { get; set; }
    }

    public class User
    {
        [BsonElement("id")]
        [JsonIgnore]
        public ObjectId Id { get; set; }

        public string Name { get; set; }
    }
}