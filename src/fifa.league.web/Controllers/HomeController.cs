﻿using System.Web.Mvc;
using fifa.league.web.Models;
using Newtonsoft.Json;

namespace fifa.league.web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

//        [HttpPost]
//        public string AddPlayer(Player player)
//        {
//            var json = JsonConvert.SerializeObject(player);
//            return json;
//        }
    }
}
