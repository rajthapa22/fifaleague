﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using fifa.league.web.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace fifa.league.web.Controllers.api
{
    public class UserController : Controller
    {
        private MongoClient _client;
        private MongoServer _server;
        private MongoDatabase _db;

        public UserController()
        {
             _client = new MongoClient("mongodb://rthapa:rthapa@ds036648.mongolab.com:36648/fifaleague");
            _server = _client.GetServer();
            _db = _server.GetDatabase("fifaleague");
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}
