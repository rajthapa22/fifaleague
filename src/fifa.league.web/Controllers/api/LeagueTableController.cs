﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using fifa.league.web.Models;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace fifa.league.web.Controllers.api
{
    public class LeagueTableController : ApiController
    {
        public MongoClient Client { get; private set; }
        public MongoServer Server { get; private set; }
        public MongoDatabase Db { get; private set; }

        public LeagueTableController()
        {
            Client = new MongoClient("mongodb://rthapa:rthapa@ds036648.mongolab.com:36648/fifaleague");
            Server = Client.GetServer();
            Db = Server.GetDatabase("fifaleague");
        }


        [HttpGet]
        [Route("api/league/table")]
        public HttpResponseMessage GetLeagueTable()
        {
            var matches = Db.GetCollection<Match>("Matches").FindAll().ToList();

            var users = Db.GetCollection<User>("Users").FindAll().ToList();

            var leageuePlayers = users.Select(user => new LeaguePlayer
            {
                Name = user.Name
            }).ToList();

            foreach (var match in matches)
            {
                var player1 = leageuePlayers.Find(x => x.Name == match.Player1.Name);
                var player2 = leageuePlayers.Find(x => x.Name == match.Player2.Name);
                UpdateLeaguePlayer(player1, match, match.Player1, match.Player2);
                UpdateLeaguePlayer(player2, match, match.Player2, match.Player1);
            }


            var response = Request.CreateResponse(HttpStatusCode.OK);
            var content = new StringContent(JsonConvert.SerializeObject(leageuePlayers));

            response.Content = content;
            return response;
        }

        private static void UpdateLeaguePlayer(LeaguePlayer leaguePlayer, Match match, Player player, Player opponentPlayer)
        {
            leaguePlayer.GoalScored += player.Goals;
            leaguePlayer.GoalConceded += opponentPlayer.Goals;
            if (leaguePlayer.Name == match.Winner)
            {
                leaguePlayer.GameWon += 1;
            }
            else
            {
                leaguePlayer.GameLoss += 1;
            }

            leaguePlayer.WinPercentage = Math.Round(decimal.Parse(leaguePlayer.GameWon.ToString()) 
                / (decimal.Parse((leaguePlayer.GameWon + leaguePlayer.GameLoss).ToString())) 
                * 100, 2);
        }
    }
}
