﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using fifa.league.web.Models;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace fifa.league.web.Controllers.api
{
    public class MatchController : ApiController
    {
        public MongoClient Client { get; private set; }
        public MongoServer Server { get; private set; }
        public MongoDatabase Db { get; private set; }

        public MatchController()
        {
            Client = new MongoClient("mongodb://rthapa:rthapa@ds036648.mongolab.com:36648/fifaleague");
            Server = Client.GetServer();
            Db = Server.GetDatabase("fifaleague");
        }

        [HttpPost]
        [Route("api/match/report")]
        public HttpResponseMessage AddMatchReport(Match match)
        {
            var collection = Db.GetCollection<Match>("Matches");
            collection.Insert(match);
            
            var json = JsonConvert.SerializeObject(match);
            var content = new StringContent(json);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = content;
            return response;
        }

        [Route("api/match/history")]
        [HttpGet]
        public HttpResponseMessage History()
        {
            var history = Db.GetCollection<Match>("Matches").FindAll().ToList();

            var json = JsonConvert.SerializeObject(history);
            var content = new StringContent(json);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = content;
            return response;
        }

        [Route("api/match/history/{name}")]
        [HttpGet]
        public HttpResponseMessage Search(string name)
        {

            var players = Db.GetCollection<User>("Users").FindAll().ToList();

            var player = players.Find(x => x.Name == name.ToUpper());

            var foundMathes = new List<Match>();

            if (player != null)
            {
                var matches = Db.GetCollection<Match>("Matches").FindAll().ToList();

                foundMathes.AddRange(matches.Where(match => match.Player1.Name == player.Name || match.Player2.Name == name.ToUpper()));
            }

            var json = JsonConvert.SerializeObject(foundMathes);
            var content = new StringContent(json);

            var response = Request.CreateResponse(foundMathes.Count > 0 ? HttpStatusCode.OK : HttpStatusCode.NoContent);

            response.Content = content;
            return response;
        }

    }
}
