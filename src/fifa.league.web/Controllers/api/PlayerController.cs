﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using fifa.league.web.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace fifa.league.web.Controllers.api
{
    public class PlayerController : ApiController
    {
        private MongoClient _client;
        private MongoServer _server;
        private MongoDatabase _db;


        public PlayerController()
        {
             _client = new MongoClient("mongodb://rthapa:rthapa@ds036648.mongolab.com:36648/fifaleague");
            _server = _client.GetServer();
            _db = _server.GetDatabase("fifaleague");
        }


        [HttpPost]
        [Route("api/user/addUser")]
        public HttpResponseMessage AddPlayer(User user)
        {
            var collection = _db.GetCollection<User>("Users");
            var newUser = new BsonDocument
            {
                {"Name", user.Name.ToUpper()}
            };

            collection.Insert(newUser);

            var response = Request.CreateResponse(HttpStatusCode.OK);
            var content = new StringContent(JsonConvert.SerializeObject(user));
            response.Content = content;
            return response;
        }

        [HttpGet]
        [Route("api/user/getusers")]
        public HttpResponseMessage GetPlayers()
        {
            var collection = _db.GetCollection<User>("Users");
            List<User> players = collection.FindAllAs<User>().ToList();
            var response = Request.CreateResponse(HttpStatusCode.OK);
            var content = new StringContent(JsonConvert.SerializeObject(players));
            response.Content = content;
            return response;
        }
    }
}
